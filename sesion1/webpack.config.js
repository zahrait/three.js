module.exports = {
    mode: "production",
    entry: {
        "prac1-1": './src/prac1-1.js'
      },
    devServer: {
        writeToDisk: true
    }
};