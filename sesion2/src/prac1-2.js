import * as THREE from 'three';
import { WEBGL } from 'three/examples/jsm/WebGL.js';

if ( !WEBGL.isWebGLAvailable() ) {
    alert('ERROR');
}
const three = require('three');
const OrbitControls = require('three-orbitcontrols');
// Crear escena
const scene = new THREE.Scene();
const renderer = WEBGL.isWebGLAvailable() ? new THREE.WebGLRenderer( {antialias: true} ) : new THREE.CanvasRenderer();
renderer.setSize( window.innerWidth, window.innerHeight );
document.body.appendChild( renderer.domElement );
// crear camara
const camera = new THREE.PerspectiveCamera ( 75, window.innerWidth / window.innerHeight, 1, 10000 );
camera.position.set( 0, 0, 3 );
// var controls = new THREE.OrbitControls(camera);
const controls = new OrbitControls(camera, renderer.domElement);
controls.enableDamping = true;
controls.dampingFactor = 0.25;
controls.enableZoom = false;
controls.update();
// crear cubo
// const geometrycubo = new THREE.CubeGeometry( X, Y, Z );

const geometrycubo = new THREE.CubeGeometry( 1, 1, 1 );
const materialcubo = new THREE.MeshLambertMaterial({color: 0xff3300});
const box = new THREE.Mesh( geometrycubo, materialcubo );
// posicion
box.position.set( 1.5, 0, 0 );
box.scale.set( 1, 1, 1 );
// // rotar cubo
box.rotation.set( 0, Math.PI / 4, 0 );
// // añadir cubo a la escena
scene.add( box );


// crear esfera
// var geometryesfera = new THREE.SphereGeometry(radio de la esfera, segmentos vertical, segmentos horizontal);

var geometryesfera = new THREE.SphereGeometry(0.6, 10, 10 );
var materialesefera = new THREE.MeshLambertMaterial({color: 0xff33ff});
var sphere = new THREE.Mesh( geometryesfera, materialesefera );

// posicion
sphere.position.set( -1.5, 0, 0 );
sphere.scale.set( 1, 1, 1 );
// // rotar cubo
sphere.rotation.set( 0, Math.PI / 4, 0 );

// // añadir esfera a la escena
scene.add( sphere );




// crear cilindro
// var geometrycilindro = new THREE.CylinderGeometry(radio arriba, radio abajo, ancho, segmentos radiales);

var geometrycilindro = new THREE.CylinderGeometry(0.5, 0.5, 1, 10);
var materialcilindro = new THREE.MeshLambertMaterial({color: 0x1100ee});
var cylinder = new THREE.Mesh( geometrycilindro, materialcilindro );
// posicion
cylinder.position.set( 0, 0, 0 );
cylinder.scale.set( 1, 1, 1 );
// // rotar cubo
cylinder.rotation.set( Math.PI / 1.08, Math.PI / 1.1, Math.PI / 1.1 );
// añadir cilindro a la escena
scene.add( cylinder );

// añadir luces
var luz = new THREE.PointLight(0xff0044);
luz.position.set(120,260,100);
var luz2 = new THREE.PointLight(0x4499ff);
luz2.position.set(-100,100,200);
var luz3 = new THREE.PointLight(0x333333);
luz3.position.set(-100,-100,-100);
var luz4 = new THREE.PointLight(0x333333);
luz4.position.set(-100,-100,200);
scene.add(luz);
scene.add(luz2);
scene.add(luz3);
scene.add(luz4);
// var luz = new THREE.AmbientLight(0xffffff, 1);
// scene.add(luz);

// renderizamos
function render() {
    requestAnimationFrame(render);
    controls.update();
    
    renderer.render(scene, camera);
  };
  
  render();
// renderer.render( scene, camera );


// cuando cambias el tamaño de la ventana se actualice el render
window.addEventListener( 'resize', ( ) => {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix( );
    renderer.setSize( window.innerWidth, window.innerHeight );
    renderer.render( scene, camera );
}, false );
