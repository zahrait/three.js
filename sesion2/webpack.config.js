module.exports = {
    mode: "production",
    entry: {
        "prac1-2": './src/prac1-2.js'
      },
    devServer: {
        writeToDisk: true
    }
};